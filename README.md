# Postgres & Flask & Faker

## **Enunciado**

#### Nesta atividade, você irá construir um programa que vai te auxiliar a organizar as suas tarefas do dia-dia. O programa deverá organizar as tarefas de acordo com a matriz de Eisenhower.

Para isso, você deverá utilizar **ORM SQLAlchemy** para as queries e a biblioteca **Faker** para os dados. 

# 

`Mas o que é uma Matriz de Eisenhower?`
### 
É absolutamente normal recebermos uma enorme quantidade de tarefas para realizar e não ter nenhuma ideia de por onde começar. É aí que podemos fazer uso da Matriz de Eisenhower, é na verdade uma maneira simples de definir como priorizar as suas tarefas do dia a dia, de modo que, as tarefas recebem prioridades de 1 a 4, sendo elas: 1 - "**urgente e importante**, 2 - **não urgente e importante**, 3 - **não importante e urgente** e 4 - **não urgente e não importante**".
#### 
 ![The eisenhower matrix](https://nextlevelgents.com/wp-content/uploads/2020/04/eisenhower-matrix.jpg)
#### 
 `“O que é importante raramente é urgente e o que é urgente raramente é importante.” – Dwight Eisenhower, 34º Presidente Americano`
# 
## ****Voltando ao enunciado****

  * Crie um banco de dados denominado `DatabaseTaskOrganizer`.
  * Crie as seguintes tabelas: 

### **Categories** 
### 
  - Tem relação de 1:1 com a tabela **Task**.

  - Deve ter como parâmetros 
    - `name`
      - VARCHAR(100)
    
    - `description`.
      - TEXT

## 

### **Task**
  - Deve ter relação de 1:1 com a tabela **categories**.

  - Deve ter relação de N:N com a tabela **Eisenhower**.

  - Deve ter os seguintes atributos:

    - `id`
      - INTEGER
      - Primary Key

    - `name`
      - VARCHAR(100)

    - `description`
      - TEXT

    - `duration`
      - UNSIGNED INT
      - Em minutos

## 

### **Eisenhower**
  - Deve ter relação de N:N com a tabela **Task**.

  - Deve ter relação de 0:2 com a tabela **EnsenhowerActions**.

- Deve ter os seguintes atributos:

    - `id`
      - INTEGER
      - Primary Key

    - `type`
      - VARCHAR(100)

### 
### **EisenhowerActions**
  - Deve ter relação de 2:0 com a tabela **Enseinhower**.

  - Deve ter os seguintes atributos:

    - `id`
      - INTEGER
      - Primary Key

    - `name`
      - VARCHAR(100)
    
    - `eisenhower_importance_1`
      - INTEGER

    - `eisenhower_urgency_2`
      - INTEGER


Exemplo de body:
### 
`data: {
 'name': 'Do It First',
 'eisenhower_importance_id': 1,
 'eisenhower_urgency_id': 3
}`

| Caso 1                      | Caso 2                      | Caso 3                      | Caso 4                      |
|-----------------------------|-----------------------------|-----------------------------|-----------------------------|
| eisenhower_importance_id: 1 | eisenhower_importance_id: 2 | eisenhower_importance_id: 1 | eisenhower_importance_id: 2 |
| eisenhower_urgency_id: 3    | eisenhower_urgency_id: 4    | eisenhower_urgency_id: 4    | eisenhower_urgency_id: 3    |
| `Result`                    | `Result`                    | `Result`                    | `Result`                    |
| 'Do It First'               | 'Delete it'                 | 'Delegate it'               | 'Schedule it'               |

# 
# 

  * Crie as seguintes rotas:

    - __categories__, deve ter as seguintes rotas:
      ```http
        GET /categories
      ```
      ```http
        POST /categories
        {
            "name": "Home Routine",
            "description": "djfhnjisdbnhfi"
        }
      ```


    - __tasks__, deve ter as seguintes rotas:
      ```http
        GET /tasks
      ```
      ```http
        GET /tasks/<category_id>
        # listar tasks de uma certa categoria
      ```
      ```http
        POST /tasks
        {
            'category_id': '',
            'eisenhower_action_id': '',
            'task_name': '',
            'task_description': '',
            'task_duration': ''
        }
      ```
      ```http
        DELETE /tasks/<task_id>
      ```
      ```
        PATCH /tasks/<task_id>
        {
            'category_id': '',
            'eisenhower_action_id': '',
            'task_name': '',
            'task_description': '',
            'task_duration': ''
        }
        # Pode-se atualizar qualquer atributo de uma task (um atributo ou mais) deixando-os explícitos no corpo da requisição
      ```
    - `/eisenhower_and_actions`, e deve ter os seguinter methods:
      - GET -> Deve retornar uma lista
      - POST
      - DELETE


# Entregáveis
### Repositório
  - Código fonte
     - Commits
        - Será considerado o commit com timestamp anterior a realização da entrega no canvas  
     - arquivos/módulos de código
        - do código solução
        - dos testes utilizados
     - arquivo README.md
  - Privacidade
     - Privado
     - Incluir como desenvolvedores
        - peer coach
        - facilitador
        - instrutor
 
